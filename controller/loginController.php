<?php

class LoginController extends BaseController
{
    public function index()
    {
        $this->registry->template->title = 'Login';
        $this->registry->template->message = '';
        $this->registry->template->show( 'login' );
    }

    public function createAction()
    {
        $user = new User($_POST);

        $val = $user->login();

        if( $val == 0 )
        {
            $this->registry->template->title = 'Sign Up';
            $this->registry->template->show( 'signup' );
        }
        elseif( $val == -1 )
        {
            //proslijediti poruku da lozinka ne valja
            $this->registry->template->title = 'Login';
            $this->registry->template->message = 1;
            $this->registry->template->show( 'login' );
        }
        else
        {
            $_SESSION["user"] = "$user->username";
            header( 'Location: ' . __SITE_URL . '/index.php?rt=home' );
            exit();
        }
    }

    public function logout() {
        session_start();
        unset($_SESSION['user']);
        session_destroy();

        header( 'Location: ' . __SITE_URL . '/index.php?rt=login' );
        exit();
    }
}

?>
