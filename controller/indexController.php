<?php

class IndexController extends BaseController
{
    public function index()
    {
        //preusmjeravanje na loginController.php
        if (!isset($_SESSION['user']))
        {
            header( 'Location: ' . __SITE_URL . '/index.php?rt=login' );
        } else {
            header( 'Location: ' . __SITE_URL . '/index.php?rt=home' );
        }
    }
}
?> 