<?php

class RecipesController extends BaseController
{
  public function index()
  { }

  public function myRecipes()
  {
    $cs = new RecipesService();

    $this->registry->template->myRecipes = $cs->getMy();
    $this->registry->template->show('myRecipes');
  }

  //Dovrsiti
  public function favorite()
  {
    $cs = new RecipesService();

    $this->registry->template->favorite = $cs->getFavorite();

    $this->registry->template->show('bookmarks');

    //funkcija u recipesService.class
  }

  public function addNewRecipe()
  {
    $cs = new RecipesService();

    //Nemamo vise tablicu kategorije, pa ovo vise ne koristimo
    //$this->registry->template->categoryList = $cs->getAllCategory();

    $this->registry->template->show('newRecipe');
  }

  public function createRecipe()
  {
    $recipe = Recipe::fromPost($_POST);

    header('Location: ' . __SITE_URL . '/index.php?rt=recipes/myRecipes');
  }

  public function moreOptions()
  {
    $cs = new RecipesService();
    $this->registry->template->favorit = $cs->inFav($_GET['id_recepta'], $_SESSION['user_id']);
    $this->registry->template->recept = $cs->getById($_GET['id_recepta']);
    if (isset($_POST['ocjena']))
      $cs->processVote($_POST['ocjena'], $_GET['id_recepta']);
    if (isset($_POST['gumb']))
      $cs->bookmark($_SESSION['user_id'], $_GET['id_recepta']);
    $this->registry->template->favorit = $cs->inFav($_GET['id_recepta'], $_SESSION['user_id']);
    if (isset($_POST['izbrisi'])) {
      $cs->izbrisi($_GET['id_recepta']);
      $this->registry->template->random = $cs->getRandom();
      $this->registry->template->show('home');
    } else {
      $this->registry->template->show('recipe');
    }
  }
}
