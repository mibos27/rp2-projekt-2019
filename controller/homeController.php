<?php

class HomeController extends BaseController
{
    public function index()
    {
        $cs = new RecipesService();

        $this->registry->template->random = $cs->getRandom();
        $this->registry->template->show( 'home' );
    }

    public function searchResult()
    {   $cs = new RecipesService();

        //Nemamo vise tablicu kategorije, pa ovo vise ne koristimo
        //$this->registry->template->categoryList = $cs->getAllCategory();
        if(isset($_POST['oznaceno']))
        {
            $oznKategorije = $_POST['oznaceno'];
            if(empty($oznKategorije)) 
            {
                //Ovjde nikad  ne bi trebo doc osim kad steka
                $this->registry->template->recipeList = $cs->getAll();
                $this->registry->template->show( 'home_search' );
            } else 
            {   
                if(!empty($_POST['pretraga'])){
                    $tmp = $cs->getByCategorys($oznKategorije);
                    $this->registry->template->recipeList = $cs->filterByName($tmp,$_POST['pretraga']);
                }else 
                {
                    $this->registry->template->recipeList = $cs->getByCategorys($oznKategorije);
                }
                $this->registry->template->show( 'home_search' );
            }
        } else 
        {
            if(!empty($_POST['pretraga'])){
                $tmp = $cs->getAll();
                $this->registry->template->recipeList = $cs->filterByName($tmp,$_POST['pretraga']);
            }else 
            {
                $this->registry->template->recipeList = $cs->getAll();
            }
            $this->registry->template->show( 'home_search' );
        }
    }
}

?>
