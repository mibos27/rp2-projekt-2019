<?php

class SignupController extends BaseController
{
    public function index()
    {
        $this->registry->template->title = 'Sign Up';
        $this->registry->template->show( 'signup' );
    }

    public function createAction()
    {
        $user = new User($_POST);

        $val = $user->save();

        if( $val == 1) 
        {
            $this->registry->template->title = 'Login';
            $this->registry->template->message = 11;
            $this->registry->template->show( 'login' );
        }
        else 
        {
            //treba proslijediti poruku, da korisnik vec postioji
            $this->registry->template->title = 'Login';
            $this->registry->template->message = 13;
            $this->registry->template->show( 'login' );            
        }
    }
}

?>