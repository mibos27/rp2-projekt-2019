# RP2 projekt 2019

Web aplikacija s receptima
Aplikacija sluzi korisnicima kako bi spremali svoje recepte, pregledavali tuđe te ih spremali kao favorite. Recepti se mogu ocjenjivati ocjenama od 1-5, te se mogu komentirati. Recepte je moguce sortirati prema određenom sastojku. Svaki od recepata pripada nekoj kategoriji (npr. deserti, hladna jela, finger food, ...), koja nije jedinstvena, odnosno jedan recept može biti u više kategorija. Neke od kategorija unaprijed su predefinirane, ali postoji mogućnost dodavanja novih (od strane administratora), te postoji mogućnost sortiranja i filtriranja po kategorijama. Aplikacija prilikom pokretanja, svaki dan pokazuje slučajna 3 recepta iz 3 različite kategorije, neovisno o broju komentara i ocjeni koju taj recept ima.

