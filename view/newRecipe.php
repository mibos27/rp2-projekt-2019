<?php require_once __SITE_PATH . '/view/_headerApp.php'; ?>
<form method="POST" action="<?php echo __SITE_URL; ?>/index.php?rt=recipes/createRecipe" enctype="multipart/form-data">


    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-9">
            <input required="required" placeholder="NAZIV RECEPTA" id="name" name="name" type="text" /> <br />
        </div>
    </div>

    <p>
        <?php
        $categoryList = ["vegetarijansko", "morsko", "desert", "meso", "tjestenina", "riža", "umak"];
        foreach ($categoryList as $category) {
            echo '<div class="form-group form-check">' .
                '<input class="form-check-input" type="checkbox" name="oznaceno[]" value="' .
                $category .
                '">' .
                '<label class="form-check-label check" for="' .
                $category . '">' . $category . '</label>' .
                '</div>';
        }
        ?>
    </p>

    <label class="labela" for="sastojci">Ovdje unesite sastojke: </label>
    <div class="row" name="sastojci">
        <div class="col-md-2">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
        </div>
        <div class="col-md-2">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
        </div>
        <div class="col-md-2">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
        </div>
        <div class="col-md-2">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
        </div>
        <div class="col-md-2">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
        </div>
        <div class="col-md-2">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
            <input class="sastojci" type="text" id="sastojak" name="sastojci[]">
        </div>
    </div>

    <div class="row textarea">
        <div class="col-md-1">
        </div>
        <textarea required="required" placeholder="Ovdje upisite postupka izrade vaseg recepta." name="postupak" id="postupak" rows="30" cols="100"></textarea>
    </div>

    <p>
        <label class="labela" for="image">Postavite sliku</label> <br />
        <input type="file" name="file" id="fileToUpload">
    </p>
    
    <div class="row">
        <button type="submit" name="submit" class="btn btn-dark col-12 col-sm-2"> Dodaj </button>
    </div>
</form>

<?php require_once __SITE_PATH . '/view/_footer.php'; ?>