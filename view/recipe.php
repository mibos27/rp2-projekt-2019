<?php require_once __SITE_PATH . '/view/_headerApp.php'; ?>

<?php
echo "<div class='row'><h2 class='recipe-name'>" . $recept->name . "</h2> </div>  <div class='row'> 
<div class='col-md-6'><span class='recept'>Kategorije:</span>";

echo "<ul class='recept-lista'>";
foreach ($recept->category as $kategorija) {
  echo "<li>" . $kategorija . "</li>";
}
echo "</ul></div>";

echo "<div class='col-md-6'><span class='recept'>Sastojci:</span> <ul class='recept-lista'>";
foreach ($recept->ingredient as $sastojak) {
  echo "<li>" . $sastojak . "</li>";
}
echo "</ul></div></div>";
echo "<div class='row'><div class='col-md-12'><span class='recept'>Priprema:</span></div>  <p class='col-md-8 recept-lista'>" . $recept->postupak;
if ($recept->slika === "app/boot/slike/") {
  echo '</p><img class="recipe-image" alt="" src="' . $recept->slika . 'default.jpg" height=300 width=300></div>';
} else {
  echo '</p><img class="recipe-image" alt="" src="' . $recept->slika . '" height=300 width=300></div>';
}
?>

<br />


<span class="recept">Ocijeni ovaj recept:</span>


<form method="POST" action="<?php echo __SITE_URL; ?>/index.php?rt=recipes/moreOptions&id_recepta=<?php echo $recept->id ?>">
  <div class="custom col-12 col-sm-3">
    <select name="ocjena">
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
    </select>
  </div>
  <div class="row">
    <button type="submit" class="btn btn-dark col-12 col-sm-3" name="submit"><span>Ocijeni</span></button>
  </div>
</form>

<br />

<?php
if ($favorit == 0) {
  ?>
  <form method="POST" action="<?php echo __SITE_URL; ?>/index.php?rt=recipes/moreOptions&id_recepta=<?php echo $recept->id ?>">
    <button type="submit" name="gumb" class="btn btn-dark"><span>Dodaj u favorite</span></button>
  </form>

<?php

}
echo "<br>";
//gumb za brisanje
if ($recept->id_user == $_SESSION['user_id'] || $_SESSION["user"] == 'admin') {
  ?>
  <form method="POST" action="<?php echo __SITE_URL; ?>/index.php?rt=recipes/moreOptions&id_recepta=<?php echo $recept->id ?>">
    <button type="submit" name="izbrisi" class="btn btn-dark"><span>Izbriši recept</span></button>
  </form>
<?php
}
?>

<?php require_once __SITE_PATH . '/view/_footer.php'; ?>