<?php require_once __SITE_PATH . '/view/_headerApp.php'; ?>

<!-- <div class="row">
  <div class="col-md-9">
  </div>
  <div class="col-md-3">
    <p class="welcome">
      Dobro dosli
      <span><?php echo $_SESSION["user"]; ?></span>!
    </p>
  </div>
</div> -->


<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <form method="POST" action="<?php echo __SITE_URL; ?>/index.php?rt=home/searchResult">

      <div class="form-group row">
        <div class="col-md-3">
        </div>
        <div class="col-md-3">
          <label class="labela" for="pretraga">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pretraga:</label>
        </div>
        <input placeholder="pretrazi recepte" class="form-control" id="pretraga" name="pretraga" type="text" /> <br />

      </div>
  </div>
</div>

<div>
  <?php
  $categoryList = ["vegetarijansko", "morsko", "desert", "meso", "tjestenina", "riza", "umak"];
  foreach ($categoryList as $category) {
    echo '<div class="form-group form-check">' .
      '<input class="form-check-input" type="checkbox" name="oznaceno[]" value="' .
      $category .
      '">' .
      '<label class="form-check-label check" for="' .
      $category . '">' . $category . '</label>' .
      '</div>';
  }
  ?>
</div>
<br />
<div class="row">
  <button type="submit" class="btn btn-dark col-12 col-sm-3" name="submit"><span>Pretraži</span></button>
  <button type="reset" class="btn btn-dark col-12 col-sm-3"><span>Odustani</span></button>
</div>
</form>

<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-9">
    <table>

      <?php
      foreach ($recipeList as $recipe) {
        echo '<tr>';
        echo '<td>';
        if ($recipe->slika === "app/boot/slike/") {
          echo '<img class="recipe-image" alt="" src="' . $recipe->slika . 'default.jpg" height=200 width=200>';
        } else {
          echo '<img class="recipe-image" alt="" src="' . $recipe->slika . '" height=200 width=200>';
        }
        echo '</td>';
        echo '<td>';
        echo ' <a href="' . __SITE_URL . '/index.php?rt=recipes/moreOptions&id_recepta=' . $recipe->id . '"><h2>' . $recipe->name . '</h2></a>';
        echo '</td>';
        echo '</tr>';
      }
      ?>

    </table>
  </div>
</div>



<?php require_once __SITE_PATH . '/view/_footer.php'; ?>