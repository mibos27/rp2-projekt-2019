<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Kuhanje s ljubavlju</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="js/script.js"></script> 
</head>
<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<ul class="navbar-nav nav-fill w-100">
			<li class="nav-item active"><a class="nav-link nav-text" href="<?php echo __SITE_URL; ?>/index.php?rt=home">Pocetna</a></li>
			<li class="nav-item active"><a class="nav-link nav-text" href="<?php echo __SITE_URL; ?>/index.php?rt=recipes/myRecipes">Moji recepti</a></li>
			<li class="nav-item active"><a class="nav-link nav-text" href="<?php echo __SITE_URL; ?>/index.php?rt=recipes/favorite">Favoriti</a></li>
			<li class="nav-item active"><a class="nav-link nav-text" href="<?php echo __SITE_URL; ?>/index.php?rt=recipes/addNewRecipe">Dodaj novi recept</a></li>
			<li class="nav-item active"><a class="nav-link nav-text" href="<?php echo __SITE_URL; ?>/index.php?rt=login/logout">Logout</a></li>		
		</ul>
	</nav>
