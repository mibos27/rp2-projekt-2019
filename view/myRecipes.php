<?php require_once __SITE_PATH . '/view/_headerApp.php'; ?>

<div class="row">
  <div class="col-md-3">
  </div>

  <?php
  echo "<table class='recepti-table'>
 <tr class='recepti-header'>
   <th></th>
   <th></th>
   <th>Ocjena</th>
 </tr>";
  foreach ($myRecipes as $recipe) {
    echo '<tr>';
    echo '<td>';
    if ($recipe->slika === "app/boot/slike/") {
      echo '<img class="recipe-image" alt="" src="' . $recipe->slika . 'default.jpg" height=150 width=150>';
    } else {
      echo '<img class="recipe-image" alt="" src="' . $recipe->slika . '" height=150 width=150>';
    }
    echo '</td>';
    echo '<td>';
    echo ' <a href="' . __SITE_URL . '/index.php?rt=recipes/moreOptions&id_recepta=' . $recipe->id . '"><span class="table-recept">' . $recipe->name . '</span></a>';
    echo '</td>';
    if ($recipe->rating == 0)
      echo '<td><span class="rating">' . $recipe->rating . "</span></td>";
    else
      echo '<td><span class="rating">' . round((float) $recipe->rating / (float) $recipe->numVotes, 2) . '</span></td>';
    echo '</tr>';
  }


  ?>

  </table>
</div>

<?php require_once __SITE_PATH . '/view/_footer.php'; ?>