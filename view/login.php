<?php require_once __SITE_PATH . '/view/_header.php'; ?>

<div class="row"><br /><br /></div>
<div class="wrapper fadeInDown">
    <div id="formContent">

        <!-- Icon -->
        <div class="fadeIn first">
            <img src="css/login.png" id="icon" alt="User Icon" />
        </div>

        <!-- Login Form -->
        <form method="POST" action="<?php echo __SITE_URL; ?>/index.php?rt=login/createAction">

            <!-- <label class="labela">Korisnicko ime</label> -->
            <input id="username" name="username" type="text" required="required" class="fadeIn second" placeholder="username" /> <br />


            <!-- <label class="labela">Lozinka</label> -->
            <input id="password" name="password" type="password" required="required" class="fadeIn third" placeholder="password" />

            <br />
            <button class="fadeIn fourth" type="submit" name="submit"><span>Login</span></button>
            <button class="fadeIn fourth" type="reset"><span>Odustani</span></button>
        </form>
        <br />
        <form method="POST" action="<?php echo __SITE_URL; ?>/index.php?rt=signup">
            <button class="fadeIn fourth" type="menu" name="signup"><span>Sign Up</span></button>
        </form>
    </div>
    <h2><?php if ($message === 1) {
            echo ('<script language="javascript">');
            echo 'alert("Korisnicka lozinka je neispravna, pokusajte ponovo.")'; 
            echo '</script>';
        }
        if ($message === 11){
            echo('<script language="javascript">');
            echo 'alert("Korisnik je uspjesno kreiran.")';  
            echo '</script>';
        }
        if ($message === 13){
            echo('<script language="javascript">');
            echo 'alert("Korisnik vec postoji.")';  //not showing an alert box.
            echo '</script>';
        }
        ?></h2>
</div>

<?php require_once __SITE_PATH . '/view/_footer.php'; ?>