<?php require_once __SITE_PATH . '/view/_header.php'; ?>

<div class="row"><br /><br /></div>
<div class="wrapper fadeInDown">
    <div id="formContent">
        
        <!-- Icon -->
        <div class="fadeIn first">
            <img src="css/login.png" id="icon" alt="User Icon" />
        </div>

        <form method="POST" action="<?php echo __SITE_URL; ?>/index.php?rt=signup/createAction">
            
                <!-- <label>Korisnicko ime</label> -->
                <input placeholder="username" type="text" id="newuser" name="username" required="required" class="fadeIn first"/><br />
            
            
                <!-- <label>E-mail adresa</label> -->
                <input placeholder="a@b.com" type="email" id="newemail" name="email" required="required" class="fadeIn second"/><br />
            
            
                <!-- <label>Lozinka</label> -->
                <input placeholder="password" id="newpassword" name="password" type="password" required="required" class="fadeIn third"/>
            
            <br />
            
                <button class="fadeIn fourth" type="submit" name="submit"><span>Sign Up</span></button>
                <button class="fadeIn fourth" type="reset"><span>Odustani</span></button>
            
        </form>

        <?php require_once __SITE_PATH . '/view/_footer.php'; ?>