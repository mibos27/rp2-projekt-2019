<?php

class Recipe
{
    public $id, $id_user, $name, $postupak, $rating, $numVotes, $slika;
    public $category = array();
    public $ingredient = array();

    function __construct(
        $id,
        $id_user,
        $name,
        $postupak,
        $rating,
        $numVotes,
        $slika,
        $category,
        $ingredient
    ) {
        //rating je ukupno bodova

        $this->id = $id;
        $this->id_user = $id_user;
        $this->name = $name;
        $this->postupak = $postupak;
        $this->rating = $rating;
        $this->numVotes = $numVotes;
        $this->slika = $slika;
        //dobivano kao polje kategorija koje su oznacene kao 1 ili 0
        $this->category = $category;
        //dobivamo kao polje sastojaka, koje onda rasporedujemo u bazu, svaki u svoj stupac
        $this->ingredient = $ingredient;
    }

    public function fromPost($data)
    {
        //upload slike
        //$data je prosljedeni $_POST

        if (isset($data['submit'])) {
            $filepath = "app/boot/slike/" . $_FILES["file"]["name"];
            echo $filepath;
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $filepath)) {
                chmod($filepath, 0755);
                //echo "<img src=" . $filepath . " height=200 width=300 />";
            } else {
                echo "Error !!";
            }
        }

        $image = $filepath;
        $name = $data['name'];
        $id_user = $_SESSION['user_id'];
        $postupak = $data['postupak'];
        $rating = 0;
        $numVotes = 0;
        $category = $data['oznaceno'];
        $ingredient = $data['sastojci'];

        for ($i = 0; $i < 7; $i++) {
            $kategorije[$i] = 0;
        }

        if (in_array("vegetarijansko", $category)) {
            $kategorije[0] = 1;
        }
        if (in_array("morsko", $category)) {
            $kategorije[1] = 1;
        }
        if (in_array("desert", $category)) {
            $kategorije[2] = 1;
        }
        if (in_array("meso", $category)) {
            $kategorije[3] = 1;
        }
        if (in_array("tjestenina", $category)) {
            $kategorije[4] = 1;
        }
        if (in_array("riza", $category)) {
            $kategorije[5] = 1;
        }
        if (in_array("umak", $category)) {
            $kategorije[6] = 1;
        }

        for ($i = 0; $i < 30; $i++) {
            if ($ingredient[$i] == "") {
                $ingredient[$i] = 0;
            }
        }

        $db = DB::getConnection();

        try {
            $st = $db->prepare('SELECT * FROM project_recipes WHERE name=:name');
            $st->execute(array('name' => $name));
        } catch (PDOException $e) {
            exit('PDO error ' . $e->getMessage());
        }

        if ($st->rowCount() > 0) {
            // Taj korisnik već postoji
            //Preusmjerit na login, cini se da radi dobro
            return 0;
        } else {
            try {
                $st = $db->prepare('INSERT INTO project_recipes(id_user, name, vegetarijansko, morsko, desert, meso, tjestenina, riza,
            umak, rating, votesNum, ingredient_1, ingredient_2, ingredient_3, ingredient_4,
            ingredient_5, ingredient_6, ingredient_7, ingredient_8, ingredient_9, ingredient_10, ingredient_11, ingredient_12, ingredient_13,
            ingredient_14, ingredient_15, ingredient_16, ingredient_17, ingredient_18, ingredient_19, ingredient_20, ingredient_21, ingredient_22,
            ingredient_23, ingredient_24, ingredient_25, ingredient_26, ingredient_27, ingredient_28, ingredient_29, ingredient_30, postupak, slika)
            VALUES ( :id_user, :name, :vegetarijansko, :morsko, :desert, :meso, :tjestenina, :riza,
            :umak, :rating, :votesNum, :ingredient_1, :ingredient_2, :ingredient_3, :ingredient_4,
            :ingredient_5, :ingredient_6, :ingredient_7, :ingredient_8, :ingredient_9, :ingredient_10, :ingredient_11, :ingredient_12, :ingredient_13,
            :ingredient_14, :ingredient_15, :ingredient_16, :ingredient_17, :ingredient_18, :ingredient_19, :ingredient_20, :ingredient_21, :ingredient_22,
            :ingredient_23, :ingredient_24, :ingredient_25, :ingredient_26, :ingredient_27, :ingredient_28, :ingredient_29, :ingredient_30, :postupak, :slika)');

            $st->execute(array(
                'id_user' => $id_user, 'name' => $name, 'vegetarijansko' => $kategorije[0], 'morsko' => $kategorije[1],
                'desert' => $kategorije[2], 'meso' => $kategorije[3], 'tjestenina' => $kategorije[4], 'riza' => $kategorije[5], 'umak' => $kategorije[6], 'rating' => '0', 'votesNum' => '0',
                'ingredient_1' => $ingredient[0], 'ingredient_2' => $ingredient[2], 'ingredient_3' => $ingredient[3], 'ingredient_4' => $ingredient[4],
                'ingredient_5' => $ingredient[5], 'ingredient_6' => $ingredient[5], 'ingredient_7' => $ingredient[6], 'ingredient_8' => $ingredient[7],
                'ingredient_9' => $ingredient[8], 'ingredient_10' => $ingredient[9], 'ingredient_11' => $ingredient[10], 'ingredient_12' => $ingredient[11],
                'ingredient_13' => $ingredient[12], 'ingredient_14' => $ingredient[13], 'ingredient_15' => $ingredient[14], 'ingredient_16' => $ingredient[15], 'ingredient_17' => $ingredient[16],
                'ingredient_18' => $ingredient[17], 'ingredient_19' => $ingredient[18], 'ingredient_20' => $ingredient[19], 'ingredient_21' => $ingredient[20], 'ingredient_22' => $ingredient[21], 'ingredient_23' => $ingredient[22],
                'ingredient_24' => $ingredient[23], 'ingredient_25' => $ingredient[24], 'ingredient_26' => $ingredient[25], 'ingredient_27' => $ingredient[26], 'ingredient_28' => $ingredient[27], 'ingredient_29' => $ingredient[28],
                'ingredient_30' => $ingredient[29], 'postupak' => $postupak, 'slika' => $image
            ));
            } catch (PDOException $e) {
                exit('PDO error ' . $e->getMessage());
            }
        }
    }
}
