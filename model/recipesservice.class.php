<?php

class RecipesService
{
    function getFavorite()
    {
        //povuci favorite iz baze
        $db = DB::getConnection();

        try {
            $st = $db->prepare('SELECT * FROM project_bookmarks WHERE id_user LIKE :id_user');
            $st->execute(array('id_user' => $_SESSION['user_id']));
        } catch (PDOException $e) {
            exit('PDO error ' . $e->getMessage());
        }

        $favoriti_id = array();
        // punimo favoriti_id sa id-om favorita
        while ($row = $st->fetch()) {
            $favoriti_id[] = $row['id_recipe'];
        }

        $db = DB::getConnection();
        try {
            $st = $db->prepare('SELECT * FROM project_recipes');
            $st->execute();
        } catch (PDOException $e) {
            exit('PDO error ' . $e->getMessage());
        }

        $recepti = array();
        $kategorije = array();

        while ($row = $st->fetch()) {
            if (in_array($row['id'], $favoriti_id)) {
                // dodavanje kategorija
                if ($row['vegetarijansko'] == 1)
                    $kategorije[] = "vegetarijansko";
                if ($row['morsko'] == 1)
                    $kategorije[] = "morsko";
                if ($row['desert'] == 1)
                    $kategorije[] = "desert";
                if ($row['meso'] == 1)
                    $kategorije[] = "meso";
                if ($row['tjestenina'] == 1)
                    $kategorije[] = "tjestenina";
                if ($row['riza'] == 1)
                    $kategorije[] = "riza";
                if ($row['umak'] == 1)
                    $kategorije[] = "umak";

                $sastojci = array();
                for ($i = 1; $i <= 30; ++$i) {
                    if ($row['ingredient_' . $i] != '0')
                        $sastojci[] = $row['ingredient_' . $i];
                }
                $recepti[] = new Recipe(
                    $row['id'],
                    $row['id_user'],
                    $row['name'],
                    $row['postupak'],
                    $row['rating'],
                    $row['votesNum'],
                    $row['slika'],
                    $kategorije,
                    $sastojci
                );
            }
        }
        return $recepti;
    }


    function getMy()
    {
        //povuci moje iz baze
        $db = DB::getConnection();
        try {
            $st = $db->prepare('SELECT * FROM project_recipes WHERE id_user LIKE :id_user');
            $st->execute(array('id_user' => $_SESSION['user_id']));
        } catch (PDOException $e) {
            exit('PDO error ' . $e->getMessage());
        }

        $recepti = array();
        //dohvaćanje kategorija
        while ($row = $st->fetch()) {
            //dohvacanje kategorija
            $kategorije = array();
            if ($row['vegetarijansko'] == 1)
                $kategorije[] = "vegetarijansko";
            if ($row['morsko'] == 1)
                $kategorije[] = "morsko";
            if ($row['desert'] == 1)
                $kategorije[] = "desert";
            if ($row['meso'] == 1)
                $kategorije[] = "meso";
            if ($row['tjestenina'] == 1)
                $kategorije[] = "tjestenina";
            if ($row['riza'] == 1)
                $kategorije[] = "riza";
            if ($row['umak'] == 1)
                $kategorije[] = "umak";




            $sastojci = array();
            for ($i = 1; $i <= 30; ++$i) {
                if ($row['ingredient_' . $i] !== '0')
                    $sastojci[] = $row['ingredient_' . $i];
            }
            $recepti[] = new Recipe(
                $row['id'],
                $row['id_user'],
                $row['name'],
                $row['postupak'],
                $row['rating'],
                $row['votesNum'],
                $row['slika'],
                $kategorije,
                $sastojci
            );
        }
        return $recepti;
    }

    function getAll()
    {
        //dohvati iz baze

        $db = DB::getConnection();

        try {
            $st = $db->prepare('SELECT * FROM project_recipes');
            $st->execute();
        } catch (PDOException $e) {
            exit('PDO error ' . $e->getMessage());
        }

        $arr = array();
        $kategorija = array();
        $namirnica = array();
        while ($row = $st->fetch()) {
            if ($row['vegetarijansko'] == 1)
                $kategorije[] = "vegetarijansko";
            if ($row['morsko'] == 1)
                $kategorije[] = "morsko";
            if ($row['desert'] == 1)
                $kategorije[] = "desert";
            if ($row['meso'] == 1)
                $kategorije[] = "meso";
            if ($row['tjestenina'] == 1)
                $kategorije[] = "tjestenina";
            if ($row['riza'] == 1)
                $kategorije[] = "riza";
            if ($row['umak'] == 1)
                $kategorije[] = "umak";


            for ($i = 1; $i <= 30; $i++) {
                if ($row['ingredient_' . $i] !== '0') {
                    $kategorija[] = $row['ingredient_' . $i];
                }
            }

            // punjenje liste $arr s receptima

            $arr[] = new Recipe(
                $row['id'],
                $row['id_user'],
                $row['name'],
                $row['rating'],
                $row['postupak'],
                $row['votesNum'],
                $row['slika'],
                $kategorija,
                $namirnica
            );
        }

        return $arr;
    }

    //vraca sve koji imaju trazene kategorije
    function getByCategorys($category)
    {
        //moze biti array
        // $category je zapravo id kategorije
        $db = DB::getConnection();
        $prepare = 'SELECT * FROM project_recipes WHERE ' . $category[0] . ' = 1';
        $N = count($category);

        for ($i = 1; $i < $N; $i++) {
            $prepare .= ' and ' . $category[$i] . ' = 1';
        }
        try {
            $st = $db->prepare($prepare);
            $st->execute();
        } catch (PDOException $e) {
            exit('PDO error ' . $e->getMessage());
        }

        $arr = array();
        $kategorija = array();
        $namirnica = array();
        while ($row = $st->fetch()) {
            if ($row['vegetarijansko'] == 1)
                $kategorije[] = "vegetarijansko";
            if ($row['morsko'] == 1)
                $kategorije[] = "morsko";
            if ($row['desert'] == 1)
                $kategorije[] = "desert";
            if ($row['meso'] == 1)
                $kategorije[] = "meso";
            if ($row['tjestenina'] == 1)
                $kategorije[] = "tjestenina";
            if ($row['riza'] == 1)
                $kategorije[] = "riza";
            if ($row['umak'] == 1)
                $kategorije[] = "umak";


            for ($i = 1; $i <= 30; $i++) {
                if ($row['ingredient_' . $i] !== '0') {
                    $kategorija[] = $row['ingredient_' . $i];
                }
            }

            // punjenje liste $arr s receptima

            $arr[] = new Recipe(
                $row['id'],
                $row['id_user'],
                $row['name'],
                $row['rating'],
                $row['postupak'],
                $row['votesNum'],
                $row['slika'],
                $kategorija,
                $namirnica
            );
        }

        return $arr;
    }


    function processVote($grade, $id_recipe)
    {

        $db = DB::getConnection();

        try {
            $st = $db->prepare('SELECT * FROM project_recipes WHERE id LIKE :id');
            $st->execute(array('id' => $id_recipe));
        } catch (PDOException $e) {
            exit('PDO error ' . $e->getMessage());
        }

        while ($row = $st->fetch()) {
            $ocjena = $row['rating'];
            $brojGlasova = $row['votesNum'];
        }
        $ocjena += $_POST['ocjena'];
        $brojGlasova += 1;

        $st = $db->prepare('UPDATE project_recipes SET rating=' . $ocjena . ', votesNum=' . $brojGlasova . ' WHERE id LIKE :id');
        $st->execute(array('id' => $id_recipe));


        return 0;
    }


    function bookmark($id_user, $id_recipe)
    {
        //ubaciti u bazu project_bookmarks
        $db = DB::getConnection();

        try {
            $st = $db->prepare('INSERT INTO project_bookmarks (id_user, id_recipe)
                                VALUES(:id_user, :id_recipe)');

            $st->execute(array('id_user' => $id_user, 'id_recipe' => $id_recipe));
        } catch (PDOException $e) {
            exit('PDO error ' . $e->getMessage());
        }
    }

    function getById($id)
    {
        $db = DB::getConnection();

        try {
            $st = $db->prepare('SELECT * FROM project_recipes WHERE id LIKE :id');
            $st->execute(array('id' => $id));
        } catch (PDOException $e) {
            exit('PDO error ' . $e->getMessage());
        }

        $kategorije = array();

        while ($row = $st->fetch()) {

            // dodavanje kategorija
            if ($row['vegetarijansko'] == 1)
                $kategorije[] = "vegetarijansko";
            if ($row['morsko'] == 1)
                $kategorije[] = "morsko";
            if ($row['desert'] == 1)
                $kategorije[] = "desert";
            if ($row['meso'] == 1)
                $kategorije[] = "meso";
            if ($row['tjestenina'] == 1)
                $kategorije[] = "tjestenina";
            if ($row['riza'] == 1)
                $kategorije[] = "riza";
            if ($row['umak'] == 1)
                $kategorije[] = "umak";

            $sastojci = array();
            for ($i = 1; $i <= 30; ++$i) {
                if ($row['ingredient_' . $i] != '0')
                    $sastojci[] = $row['ingredient_' . $i];
            }
            $recept = new Recipe(
                $row['id'],
                $row['id_user'],
                $row['name'],
                $row['postupak'],
                $row['rating'],
                $row['votesNum'],
                $row['slika'],
                $kategorije,
                $sastojci
            );
        }
        return $recept;
    }

    function filterByName($rec, $name)
    {
        $name = strtolower($name);
        $recept = array();
        foreach ($rec as $r) {

            $punoIme = strtolower($r->name);
            if (strpos($punoIme, $name) !== false) {
                $recept[] = $r;
            }
        }

        return $recept;
    }

    function getRandom()
    {
        $db = DB::getConnection();

        try {
            $st = $db->prepare('SELECT * FROM project_recipes order by RAND() LIMIT 1');
            $st->execute();
        } catch (PDOException $e) {
            exit('PDO error ' . $e->getMessage());
        }

        while ($row = $st->fetch()) {

            // dodavanje kategorija
            if ($row['vegetarijansko'] == 1)
                $kategorije[] = "vegetarijansko";
            if ($row['morsko'] == 1)
                $kategorije[] = "morsko";
            if ($row['desert'] == 1)
                $kategorije[] = "desert";
            if ($row['meso'] == 1)
                $kategorije[] = "meso";
            if ($row['tjestenina'] == 1)
                $kategorije[] = "tjestenina";
            if ($row['riza'] == 1)
                $kategorije[] = "riza";
            if ($row['umak'] == 1)
                $kategorije[] = "umak";

            $sastojci = array();
            for ($i = 1; $i <= 30; ++$i) {
                if ($row['ingredient_' . $i] !== '0')
                    $sastojci[] = $row['ingredient_' . $i];
            }
            $recept = new Recipe(
                $row['id'],
                $row['id_user'],
                $row['name'],
                $row['postupak'],
                $row['rating'],
                $row['votesNum'],
                $row['slika'],
                $kategorije,
                $sastojci
            );
        }
        return $recept;
    }

    public function inFav($recept, $user)
    { //vraca 1 ako je vec u favoritima, 0 inace
        $db = DB::getConnection();

        try {
            $st = $db->prepare('SELECT * FROM project_bookmarks WHERE id_recipe LIKE :id');
            $st->execute(array('id' => $recept));
        } catch (PDOException $e) {
            exit('PDO error ' . $e->getMessage());
        }

        while ($row = $st->fetch()) {
            if ($row['id_user'] == $user)
                return 1;
        }
        return 0;
    }

    public function izbrisi($id)
    {
        $db = DB::getConnection();

        try {
            $st = $db->prepare('DELETE FROM project_bookmarks WHERE id_recipe LIKE :id');
            $st->execute(array('id' => $id));
        } catch (PDOException $e) {
            exit('PDO error ' . $e->getMessage());
        }

        try {
            $st = $db->prepare('DELETE FROM project_recipes WHERE id LIKE :id');
            $st->execute(array('id' => $id));
        } catch (PDOException $e) {
            exit('PDO error ' . $e->getMessage());
        }
    }
}
