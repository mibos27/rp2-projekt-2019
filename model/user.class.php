<?php

class User
{
	public $id, $username, $email, $password;

	function __construct( $data )
	{
        foreach ($data as $key => $value)
        {
            $this->$key = $value;
        }
	}

	function __get( $prop ) { return $this->$prop; }
    function __set( $prop, $val ) { $this->$prop = $val; return $this; }


    function save()
    {
        //projerava je li korisnik vec u bazi
        $db = DB::getConnection();
        $password_hash = password_hash($this->password, PASSWORD_DEFAULT);

        try
        {
            $st = $db->prepare( 'SELECT * FROM project_users WHERE username=:username' );
            $st->execute( array( 'username' => $_POST["username"] ) );
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        if( $st->rowCount() > 0 )
        {
            // Taj korisnik već postoji
            //Preusmjerit na login, cini se da radi dobro
            return 0;
        }
        else
        {
            try
            {
                $st = $db->prepare( 'INSERT INTO project_users (username, password_hash, email)
                                VALUES(:username, :password_hash, :email)' );

                $st->execute( array ('username' => $this->username, 'password_hash' => $password_hash, 'email' => $this->email ) );
            }
            catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }
            return 1;
        }
    }

    function login()
    {
        $db = DB::getConnection();

        try
        {
            $st = $db->prepare( 'SELECT password_hash,id FROM project_users WHERE username=:username' );
            $st->execute( array( 'username' => $this->username ) );
        }
        catch( PDOException $e ) { exit( 'PDO error ' . $e->getMessage() ); }

        $row = $st->fetch();

        if( $row === false )
        {
            //Treba preusmjerit na signup
            // Taj user ne postoji, upit u bazu nije vratio ništa.
            return 0;
        }
        else
        {
            // Postoji user. Dohvati hash njegovog passworda.
            $hash = $row[ 'password_hash' ];

            // Da li je password dobar?
            if( password_verify( $this->password, $hash ) )
            {		$_SESSION["user_id"] = $row['id'];
                //To radi dobro
                // Dobar je. Ulogiraj ga.
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }
}

?>
