<?php

require_once '../../model/db.class.php';

$db = DB::getConnection();

$has_tables = false;

try 
{
    $st = $db->prepare(
        'SHOW TABLES LIKE :tblname'
    );

    $st->execute( array ( 'tblname' => 'project_users' ) );
    if( $st->rowCount() > 0)
        $has_tables = true;

    $st->execute( array ( 'tblname' => 'project_recipes' ) );
    if( $st->rowCount() > 0)
        $has_tables = true;

    $st->execute( array ( 'tblname' => 'project_bookmarks' ) );
    if( $st->rowCount() > 0)
        $has_tables = true;
}
catch ( PDOException $e ) { exit( "PDO error [show tables]: " . $e->getMessage() ); }

if( $has_tables )
{
    exit( 'Tablice project_users / project_recipes / project_bookmark vec postoje.
    Obrisite pa probajete ponovo.' );
}

//CREATE TABLES
try
{
    $st = $db->prepare(
        'CREATE TABLE IF NOT EXISTS project_users (' .
        'id int NOT NULL PRIMARY KEY AUTO_INCREMENT,' .
        'username varchar(50) NOT NULL, ' .
        'password_hash varchar(255) NOT NULL,' .
        'email varchar(50) NOT NULL)'
    );

    $st->execute();
}
catch( PDOException $e ) { exit( "PDO error [create project_users]: " . $e->getMessage() ); }

echo "Napravio tablicu project_users.<br />";

try
{
    $st = $db->prepare(
        'CREATE TABLE IF NOT EXISTS project_recipes (' .
        'id int NOT NULL PRIMARY KEY AUTO_INCREMENT,' .
        'id_user int NOT NULL, ' .
        'name varchar(50) NOT NULL,' .
        'vegetarijansko int,' .
        'morsko int,' .
        'desert int,' .
        'meso int,' .
        'tjestenina int,' .
        'riza int,' .
        'umak int,' .
        'rating int NOT NULL,' .
        'votesNum int,' .
        'ingredient_1 varchar(50),' .
        'ingredient_2 varchar(50),' .
        'ingredient_3 varchar(50),' .
        'ingredient_4 varchar(50),' .
        'ingredient_5 varchar(50),' .
        'ingredient_6 varchar(50),' .
        'ingredient_7 varchar(50),' .
        'ingredient_8 varchar(50),' .
        'ingredient_9 varchar(50),' .
        'ingredient_10 varchar(50),' .
        'ingredient_11 varchar(50),' .
        'ingredient_12 varchar(50),' .
        'ingredient_13 varchar(50),' .
        'ingredient_14 varchar(50),' .
        'ingredient_15 varchar(50),' .
        'ingredient_16 varchar(50),' .
        'ingredient_17 varchar(50),' .
        'ingredient_18 varchar(50),' .
        'ingredient_19 varchar(50),' .
        'ingredient_20 varchar(50),' .
        'ingredient_21 varchar(50),' .
        'ingredient_22 varchar(50),' .
        'ingredient_23 varchar(50),' .
        'ingredient_24 varchar(50),' .
        'ingredient_25 varchar(50),' .
        'ingredient_26 varchar(50),' .
        'ingredient_27 varchar(50),' .
        'ingredient_28 varchar(50),' .
        'ingredient_29 varchar(50),' .
        'ingredient_30 varchar(50),' .
        'postupak text NOT NULL,' .
        'slika text)'
    );
    
    $st->execute();
}
catch( PDOException $e ) { exit( "PDO error [create project_recipes]: " . $e->getMessage() ); }

echo "Napravio tablicu project_recipies.<br />";

try
{
    $st = $db->prepare(
        'CREATE TABLE IF NOT EXISTS project_bookmarks (' .
        'id_user int NOT NULL,' .
        'id_recipe int NOT NULL)'
    );
    
    $st->execute();
}
catch( PDOException $e ) { exit( "PDO error [create project_bookmarks]: " . $e->getMessage() ); }

echo "Napravio tablicu project_bookmarks.<br />";

//TABLES DATA

//korisnici
try
{
    $st = $db->prepare ( 'INSERT INTO project_users(username, password_hash, email) VALUES ( :username, :password, \'a@b.com\')' );

    $st->execute( array( 'username' => 'ana', 'password' => password_hash( 'ana123' , PASSWORD_DEFAULT ) ) );
    $st->execute( array( 'username' => 'matko', 'password' => password_hash( 'matko123' , PASSWORD_DEFAULT ) ) );
    $st->execute( array( 'username' => 'slaven', 'password' => password_hash( 'slaven123' , PASSWORD_DEFAULT ) ) );
    $st->execute( array( 'username' => 'vid', 'password' => password_hash( 'vid123' , PASSWORD_DEFAULT ) ) );
    $st->execute( array( 'username' => 'matija', 'password' => password_hash( 'matija123' , PASSWORD_DEFAULT ) ) );
    $st->execute( array( 'username' => 'stipe', 'password' => password_hash( 'stipe123' , PASSWORD_DEFAULT ) ) );
    $st->execute( array( 'username' => 'admin', 'password' => password_hash( 'admin' , PASSWORD_DEFAULT ) ) );

}
catch( PDOException $e ) { exit( "PDO error [insert project_users]: " . $e->getMessage() ); }

echo "Ubacio u tablicu project_users.<br />";


//recepti
try
{
    $st = $db->prepare( 'INSERT INTO project_recipes(id_user, name, vegetarijansko, morsko, desert, meso, tjestenina, riza,
                        umak, rating, votesNum, ingredient_1, ingredient_2, ingredient_3, ingredient_4, 
                        ingredient_5, ingredient_6, ingredient_7, ingredient_8, ingredient_9, ingredient_10, ingredient_11, ingredient_12, ingredient_13, 
                        ingredient_14, ingredient_15, ingredient_16, ingredient_17, ingredient_18, ingredient_19, ingredient_20, ingredient_21, ingredient_22, 
                        ingredient_23, ingredient_24, ingredient_25, ingredient_26, ingredient_27, ingredient_28, ingredient_29, ingredient_30, postupak, slika)
                        VALUES ( :id_user, :name, :vegetarijansko, :morsko, :desert, :meso, :tjestenina, :riza,
                        :umak, :rating, :votesNum, :ingredient_1, :ingredient_2, :ingredient_3, :ingredient_4, 
                        :ingredient_5, :ingredient_6, :ingredient_7, :ingredient_8, :ingredient_9, :ingredient_10, :ingredient_11, :ingredient_12, :ingredient_13, 
                        :ingredient_14, :ingredient_15, :ingredient_16, :ingredient_17, :ingredient_18, :ingredient_19, :ingredient_20, :ingredient_21, :ingredient_22, 
                        :ingredient_23, :ingredient_24, :ingredient_25, :ingredient_26, :ingredient_27, :ingredient_28, :ingredient_29, :ingredient_30, :postupak, :slika)' );
    
    $st->execute( array( 'id_user' => '5', 'name' => 'Umak za tjesteninu od spinata i sampinjona', 'vegetarijansko' => '0', 'morsko' => '0',
                         'desert' => '0', 'meso' => '1', 'tjestenina' => '1', 'riza' => '0', 'umak' => '1', 'rating' => '0', 'votesNum' => '0',
                        'ingredient_1' => '500g spinata', 'ingredient_2' => '300g sampinjona', 'ingredient_3' => '50g maslaca', 'ingredient_4' => '2 zlice ribanca', 
                        'ingredient_5' => 'pola sake kikirikija', 'ingredient_6' => '100g budole', 'ingredient_7' => 'sok od pola limuna', 'ingredient_8' => 'malo papra', 
                        'ingredient_9' => 'sol po potrebi', 'ingredient_10' => '2 zlicice gustina', 'ingredient_11' => 'saka susenih rajcica', 'ingredient_12' => 'kajenski papar', 
                        'ingredient_13' => '300g kuhane tjestenine(po volji)', 'ingredient_14' => '0', 'ingredient_15' => '0', 'ingredient_16' => '0', 'ingredient_17' => '0',
                        'ingredient_18' => 'srednji luk', 'ingredient_19' => '0', 'ingredient_20' => '0', 'ingredient_21' => '0', 'ingredient_22' => '0', 'ingredient_23' => '0', 
                        'ingredient_24' => '0', 'ingredient_25' => '0', 'ingredient_26' => '0', 'ingredient_27' => '0', 'ingredient_28' => '0', 'ingredient_29' => '0',
                        'ingredient_30' => '0', 'postupak' => 'Na maslacu kratko poprziti luk do zlatne boje pa dodati budolu i sampinjone narezane po volji. Kad se sampinjoni dovoljno prodinstaju, dodati spinat i sve zajedno dinstati.
                        Zatim dodati kikiriki i suhu rajcicu te sok od limuna. Ostaviti da se krcka 5-10 minuta i podljevati po potrebi. Na kraju dodati gustin, uz stalno mjesanje te ribanac i zaciniti po volji. Tjesteninu skuhati posebno.
                        Jelo je za 2 osobe. Posluziti toplo.', 'slika' => 'app/boot/slike/umak_spinat.jpg'));

    $st->execute( array( 'id_user' => '2', 'name' => 'Umak od sira za nachose', 'vegetarijansko' => '1', 'morsko' => '0',
                        'desert' => '0', 'meso' => '0', 'tjestenina' => '0', 'riza' => '0', 'umak' => '1', 'rating' => '0', 'votesNum' => '0',
                       'ingredient_1' => '2 zlice maslaca', 'ingredient_2' => '2 zlice ostrog brasna', 'ingredient_3' => '1 casa mlijeka', 'ingredient_4' => 'malo soli', 
                       'ingredient_5' => 'malo chilija', 'ingredient_6' => '200g ribanog cheddara', 'ingredient_7' => '0', 'ingredient_8' => '0', 
                       'ingredient_9' => '0', 'ingredient_10' => '0', 'ingredient_11' => '0', 'ingredient_12' => '0', 
                       'ingredient_13' => '0', 'ingredient_14' => '0', 'ingredient_15' => '0', 'ingredient_16' => '0', 'ingredient_17' => '0',
                       'ingredient_18' => 'srednji luk', 'ingredient_19' => '0', 'ingredient_20' => '0', 'ingredient_21' => '0', 'ingredient_22' => '0', 'ingredient_23' => '0', 
                       'ingredient_24' => '0', 'ingredient_25' => '0', 'ingredient_26' => '0', 'ingredient_27' => '0', 'ingredient_28' => '0', 'ingredient_29' => '0',
                       'ingredient_30' => '0', 'postupak' => 'Brasno i maslac prziti na laganoj vatri u tavi. Mjesati sve dok ne postanu mjehuricasti i lagano zlatne boje, otprilike 1 munutu. U to dodati mlijeko uz stalno mjesanje 
                       sve dok mjesavina ne zakipi i postane dovoljno gusta. Tada ugasiti vatru i u mjesavinu dodati naribani sir te mjesati dok se ne otopi. Dodati zacine i posluziti toplo.', 'slika' => 'app/boot/slike/nacho_cheese.jpg'));
}
catch( PDOException $e ) { exit("PDO error [insert project_recipes]: " . $e->getMessage() ); }

echo "Ubacio u tablicu project_recipes.<br />";

//favoriti
try
{
    $st = $db->prepare( 'INSERT INTO project_bookmarks(id_user, id_recipe) VALUES (:id_user, :id_recipe)' );

    $st->execute( array( 'id_user' => '4', 'id_recipe' => '1' ) );

}
catch( PDOException $e) { exit( "PDO error [insert project_bookmarks]: " . $e.getMessage() ); }

echo "Ubacio u tablicu project_bookmarks.<br />";

?>

